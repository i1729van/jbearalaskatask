import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;



class RESTClient {
    private String rest_host;
    private String rest_port;

    RESTClient(String host, String port){
        rest_host = host;
        rest_port = port;
    }

    JSONObject readBearById(int id) throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear/" + String.valueOf(id);
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        bufferedReader.close();
        JSONTokener tokener = new JSONTokener(response.toString());
        JSONObject jsonBear = new JSONObject(tokener);
        return jsonBear;
    
    }

    String readBearByDeletedId(int id) throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear/" + String.valueOf(id);
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        bufferedReader.close();
        return response.toString();
    
    }

    JSONArray getAllBears() throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        bufferedReader.close();
        JSONArray bears = new JSONArray(response.toString());
        return bears;
    
    }

    List<Integer> getIdList() throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("GET");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        bufferedReader.close();
        JSONArray bears = new JSONArray(response.toString());
        List<Integer> bearsIdList = new ArrayList<Integer>();
        for(int i=0;i<bears.length();i++){
            bearsIdList.add(bears.getJSONObject(i).getInt("bear_id"));
        }
        return bearsIdList;
    
    }


    String deleteAllBears() throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("DELETE");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        return response.toString();
    }

    String deleteBearById(int id) throws IOException {
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear/" + String.valueOf(id);
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("DELETE");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        return response.toString();
    }

    int createBear(String bear_type, String bear_name, float bear_age) throws IOException {
        JsonObject bear_json = new JsonObject();
        bear_json.addProperty("bear_type", bear_type);
        bear_json.addProperty("bear_name", bear_name);
        bear_json.addProperty("bear_age", bear_age);
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("POST");

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
        outputStreamWriter.write(bear_json.toString());
        outputStreamWriter.flush();

        int responseCode = connection.getResponseCode();
        return responseCode;
    }

    int createBearOnlyType(String bear_type) throws IOException {
        JsonObject bear_json = new JsonObject();
        bear_json.addProperty("bear_type", bear_type);
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("POST");

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
        outputStreamWriter.write(bear_json.toString());
        outputStreamWriter.flush();

        int responseCode = connection.getResponseCode();
        return responseCode;
    }

    int createBearTypeAndNameOnly(String bear_type, String bear_name) throws IOException {
        JsonObject bear_json = new JsonObject();
        bear_json.addProperty("bear_type", bear_type);
        bear_json.addProperty("bear_name", bear_name);
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("POST");

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
        outputStreamWriter.write(bear_json.toString());
        outputStreamWriter.flush();

        int responseCode = connection.getResponseCode();
        return responseCode;
    }

    String createBearAndGetId(String bear_type, String bear_name, float bear_age) throws IOException {
        JsonObject bear_json = new JsonObject();
        bear_json.addProperty("bear_type", bear_type);
        bear_json.addProperty("bear_name", bear_name);
        bear_json.addProperty("bear_age", bear_age);
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear";
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("POST");

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
        outputStreamWriter.write(bear_json.toString());
        outputStreamWriter.flush();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while((inputLine = bufferedReader.readLine())!= null){
            response.append(inputLine);
        }
        return response.toString();
    }

    int updateBearById(int bear_id, String bear_type, String bear_name, float bear_age) throws IOException {
        JsonObject bear_json = new JsonObject();
        bear_json.addProperty("bear_type", bear_type);
        bear_json.addProperty("bear_name", bear_name);
        bear_json.addProperty("bear_age", bear_age);
        String url_path = "http://" + rest_host + ":" + rest_port + "/bear/" + String.valueOf(bear_id);
        URL url = new URL(url_path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("PUT");

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
        outputStreamWriter.write(bear_json.toString());
        outputStreamWriter.flush();

        int responseCode = connection.getResponseCode();
        return responseCode;
    }





}



