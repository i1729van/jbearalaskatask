import java.util.List;
import org.testng.Assert;
import org.testng.annotations.*;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.UnknownHostException;

public class CreateBearsTest {

    private RESTClient testClient;

    // поставщик заведомо корректных значений для записи
    @DataProvider(name = "validBears")
    public Object[][] getRandomValidBears() {
        return new Object[][]{
            {"POLAR", "Name1", 0.0},
            {"POLAR", "Name2", 99.9},
            {"BROWN", "Name3", 0.0},
            {"BROWN", "Name4", 99.9},
            {"BLACK", "Name5", 0.0},
            {"BLACK", "Name6", 99.9},
            {"GUMMY", "Name7", 0.0},
            {"GUMMY", "Name8", 99.9},
        };
    }

    // поставщик с оторицательным возрастом
    @DataProvider(name = "negativeAgeBears")
    public Object[][] getRandomNegativeAgeBears() {
        return new Object[][]{
            {"POLAR", "Name1", -1.0},
            {"POLAR", "Name2", -99.9},
            {"BROWN", "Name3", -1.0},
            {"BROWN", "Name4", -99.9},
            {"BLACK", "Name5", -1.0},
            {"BLACK", "Name6", -99.9},
            {"GUMMY", "Name7", -1.0},
            {"GUMMY", "Name8", -99.9},
        };
    }

    // поставщик только типа
    @DataProvider(name = "invalidOneParBears")
    public Object[][] getInvalidOneArgBears() {
        return new Object[][]{
            {"POLAR"},
            {"POLAR"},
            {"BROWN"},
            {"BROWN"},
            {"BLACK"},
            {"BLACK"},
            {"GUMMY"},
            {"GUMMY"},
        };
    }

    // поставщик только типа и имени
    @DataProvider(name = "invalidTwoParamsBears")
    public Object[][] getInvalidTwoArgBears() {
        return new Object[][]{
            {"POLAR", "Name1"},
            {"POLAR", "Name2"},
            {"BROWN", "Name3"},
            {"BROWN", "Name4"},
            {"BLACK", "Name5"},
            {"BLACK", "Name6"},
            {"GUMMY", "Name7"},
            {"GUMMY", "Name8"},
        };
    }

    @BeforeMethod
    @Parameters({"host", "port"})
    public void setUp(String host, String port) throws UnknownHostException{
        testClient = new RESTClient(host, port);
    }

    @AfterMethod
    public void tearDown(){}

    /**
     * Тест POST метода. Проверяем, что все значения всех свойств корректно записались
     * и соответствуют тем, что посылали
     * @param bear_type
     * @param bear_name
     * @param bear_age
     * получаются от поставщика данных 
     * Чтобы найти id (не зная чтто он получавется в ответе) необходимо взять список id ДО и ПОСЛЕ и увидеть прирощение
     */
    @Test(dataProvider = "validBears")
    public void createBearAndCheckEachPropertyTest(String bear_type, String bear_name, double bear_age){
        try{

            List<Integer> bearsIdListBefore = testClient.getIdList();

            int responseCreateCode = testClient.createBear(bear_type, bear_name, (float) bear_age);
            Assert.assertEquals(responseCreateCode, 200);

            List<Integer> bearsIdListAfter = testClient.getIdList();
            bearsIdListAfter.removeAll(bearsIdListBefore);
            int newBearId = bearsIdListAfter.get(0);

            JSONObject bearUnderTest = testClient.readBearById(newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_id"), newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_type"), bear_type);
            Assert.assertEquals(bearUnderTest.get("bear_name"), bear_name.toUpperCase());
            Assert.assertEquals(bearUnderTest.get("bear_age"), bear_age);
        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }


    /**
     * Тест POST метода. Проверяем, что при записи с одним аргументом (тип) не происходит записи в хранилище
     * @param bear_type
     * получаются от поставщика данных 
     */
    @Test(dataProvider = "invalidOneParBears")
    public void tryCreateBearWithOnlyBearTypeArgsTest(String bear_type){
        try{

            JSONArray bearsBefore = testClient.getAllBears();

            int responseCreateCode = testClient.createBearOnlyType(bear_type);
            Assert.assertEquals(responseCreateCode, 200);

            JSONArray bearsAfter = testClient.getAllBears();
            Assert.assertEquals(bearsBefore.length(), bearsAfter.length());

        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }



    /**
     * Тест POST метода. Проверяем, что при записи только с двумя из 3х полей не происходит записи в хранилище
     * и соответствуют тем, что посылали
     * @param bear_type
     * @param bear_name
     * получаются от поставщика данных 
     */
    @Test(dataProvider = "invalidTwoParamsBears")
    public void tryCreateBearWithTwoArgsArgsTest(String bear_type, String bear_name){
        try{

            JSONArray bearsBefore = testClient.getAllBears();

            int responseCreateCode = testClient.createBearTypeAndNameOnly(bear_type, bear_name);
            Assert.assertEquals(responseCreateCode, 200);

            JSONArray bearsAfter = testClient.getAllBears();
            Assert.assertEquals(bearsBefore.length(), bearsAfter.length());

        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }



     /**
     * Тест POST метода. Проверяем что при добавлении медведя с отрицательным
     * возрастом либо он добавляется с отрицательным, но лучше чтобы обнулялся 0.0
     * @param bear_type
     * @param bear_name
     * @param bear_age
     * получаются от поставщика данных 
     */
    @Test(dataProvider = "negativeAgeBears")
    public void createBearWithNegativeAgeTest(String bear_type, String bear_name, double bear_age){
        try{

            List<Integer> bearsIdListBefore = testClient.getIdList();

            int responseCreateCode = testClient.createBear(bear_type, bear_name, (float) bear_age);
            Assert.assertEquals(responseCreateCode, 200);

            List<Integer> bearsIdListAfter = testClient.getIdList();
            bearsIdListAfter.removeAll(bearsIdListBefore);
            int newBearId = bearsIdListAfter.get(0);

            JSONObject bearUnderTest = testClient.readBearById(newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_id"), newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_type"), bear_type);
            Assert.assertEquals(bearUnderTest.get("bear_name"), bear_name.toUpperCase());
            Assert.assertTrue((double)bearUnderTest.get("bear_age") == 0.0 || ((double)bearUnderTest.get("bear_age") == bear_age));
        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }

}