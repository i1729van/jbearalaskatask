import java.util.List;
import org.testng.Assert;
import org.testng.annotations.*;
import org.json.JSONObject;
import java.net.UnknownHostException;

public class UpdateBearsTest {

    private RESTClient testClient;

    // поставщик заведомо корректных значений для update теста
    @DataProvider(name = "validBears")
    public Object[][] getRandomValidBears() {
        return new Object[][]{
            {"POLAR", "Name1", 2.0,     "POLAR", "Name8", 2.0},
            {"POLAR", "Name2", 99.9,    "POLAR", "Name7", 99.9},
            {"BROWN", "Name3", 2.0,     "BROWN", "Name6", 2.0},
            {"BROWN", "Name4", 99.9,    "BROWN", "Name5", 99.9},
            {"BLACK", "Name5", 2.0,     "BLACK", "Name4", 2.0},
            {"BLACK", "Name6", 99.9,    "BLACK", "Name3", 99.9},
            {"GUMMY", "Name7", 2.0,     "GUMMY", "Name2", 2.0},
            {"GUMMY", "Name8", 99.9,    "GUMMY", "Name1", 99.9},
        };
    }


    @BeforeMethod
    @Parameters({"host", "port"})
    public void setUp(String host, String port) throws UnknownHostException{
        testClient = new RESTClient(host, port);
    }

    @AfterMethod
    public void tearDown(){}

    /**
     * Проверяем UPDATE метод
     */
    @Test(dataProvider = "validBears")
    public void updateAndCheckBearTest(String bear_type, String bear_name, double bear_age, String bear_type2, String bear_name2, double bear_age2){
        try{

            String deletedAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deletedAllOk, "OK");

            int responseCreateCode = testClient.createBear(bear_type, bear_name, (float) bear_age);
            Assert.assertEquals(responseCreateCode, 200);

            List<Integer> bearsIdList = testClient.getIdList();
            int newBearId = bearsIdList.get(0);

            JSONObject bearUnderTest = testClient.readBearById(newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_id"), newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_type"), bear_type);
            Assert.assertEquals(bearUnderTest.get("bear_name"), bear_name.toUpperCase());
            Assert.assertEquals(bearUnderTest.get("bear_age"), bear_age);

            int responseUpdateCode = testClient.updateBearById(newBearId, bear_type2, bear_name2, (float) bear_age2);
            Assert.assertEquals(responseUpdateCode, 200);

            JSONObject updatedBear = testClient.readBearById(newBearId);
            Assert.assertEquals(updatedBear.get("bear_id"), newBearId);
            Assert.assertEquals(updatedBear.get("bear_type"), bear_type2);
            Assert.assertEquals(updatedBear.get("bear_name"), bear_name2);
            Assert.assertEquals(updatedBear.get("bear_age"), bear_age2);

        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }

}