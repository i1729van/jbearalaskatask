import org.testng.Assert;
import org.testng.annotations.*;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.UnknownHostException;


public class BDeleteBearsTest {

    private RESTClient testClient;

    // поставщик заведомо корректных значений для записи
    @DataProvider(name = "validBears")
    public Object[][] getRandomValidBears() {
        return new Object[][]{
            {"POLAR", "Name1", 0.0},
            {"POLAR", "Name2", 99.9},
            {"BROWN", "Name3", 0.0},
            {"BROWN", "Name4", 99.9},
            {"BLACK", "Name5", 0.0},
            {"BLACK", "Name6", 99.9},
            {"GUMMY", "Name7", 0.0},
            {"GUMMY", "Name8", 99.9},
        };
    }


    @BeforeMethod
    @Parameters({"host", "port"})
    public void setUp(String host, String port) throws UnknownHostException{
        testClient = new RESTClient(host, port);
    }

    @AfterMethod
    public void tearDown(){}

    /**
     * простой тест на удаление всех записей
     * Удаляем, Записываем. Проверяем количество. Удаляем всех. Ожидаем 0
     */
    @Test()
    public void getBearsLengthTest(){
        try{

            String deleteAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deleteAllOk, "OK");

            int responseCreateCode = testClient.createBear("POLAR", "Name1", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("POLAR", "Name2", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BROWN", "Name3", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BROWN", "Name4", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BLACK", "Name5", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BLACK", "Name6", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("GUMMY", "Name7", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("GUMMY", "Name8", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            
            // проверяем что список увеличился на нашу величину
            final int okLength = 8;
            JSONArray afterBears = testClient.getAllBears();
            Assert.assertEquals(afterBears.length(), okLength);
            
            //удаляем всех снова
            String deleteAgainStatus = testClient.deleteAllBears();
            Assert.assertEquals(deleteAgainStatus, "OK");
            
            // проверяем что ничего не осталось
            JSONArray afterDeleteBears = testClient.getAllBears();
            Assert.assertEquals(afterDeleteBears.length(), 0);


        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }

    /**
     * тест повторяет логику из create теста, но всё же
     * 
     */
    @Test(dataProvider = "validBears")
    public void deleteBearByIdTest(String bear_type, String bear_name, double bear_age){
        try{
            // удалим всех медведей
            String deleteAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deleteAllOk, "OK");

            // проверим что ничего не осталось
            JSONArray afterDeleteBears = testClient.getAllBears();
            Assert.assertEquals(afterDeleteBears.length(), 0);

            String createdBearId = testClient.createBearAndGetId(bear_type, bear_name, (float) bear_age);
            int bearId = Integer.parseInt(createdBearId);

            JSONObject bearUnderTest = testClient.readBearById(bearId);
            Assert.assertEquals(bearUnderTest.get("bear_id"), bearId);
            Assert.assertEquals(bearUnderTest.get("bear_type"), bear_type);
            Assert.assertEquals(bearUnderTest.get("bear_name"), bear_name);
            Assert.assertEquals(bearUnderTest.get("bear_age"), bear_age);

            // убедимся что список на 1 больше (либо стал равен 1, т.к. мы всех удалили ДО)
            JSONArray bearsAfterAdd = testClient.getAllBears();
            Assert.assertEquals(bearsAfterAdd.length(), 1);

            // удаляем по известному id
            String deletedByIdStatus = testClient.deleteBearById(bearId);
            Assert.assertEquals(deletedByIdStatus, "OK");

            // пытаемся прочитать по удаленному id
            String deletedResponseWhenRead = testClient.readBearByDeletedId(bearId);
            Assert.assertEquals(deletedResponseWhenRead, "EMPTY");

            // убедимся список опять пуст
            JSONArray bearsAftedDeletedById = testClient.getAllBears();
            Assert.assertEquals(bearsAftedDeletedById, 0);

        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }

}