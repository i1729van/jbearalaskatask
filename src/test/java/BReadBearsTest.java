import java.util.List;
import org.testng.Assert;
import org.testng.annotations.*;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.UnknownHostException;

public class BReadBearsTest {

    private RESTClient testClient;

    // поставщик заведомо корректных значений для записи
    @DataProvider(name = "validBears")
    public Object[][] getRandomValidBears() {
        return new Object[][]{
            {"POLAR", "Name1", 0.0},
            {"POLAR", "Name2", 99.9},
            {"BROWN", "Name3", 0.0},
            {"BROWN", "Name4", 99.9},
            {"BLACK", "Name5", 0.0},
            {"BLACK", "Name6", 99.9},
            {"GUMMY", "Name7", 0.0},
            {"GUMMY", "Name8", 99.9},
        };
    }


    @BeforeMethod
    @Parameters({"host", "port"})
    public void setUp(String host, String port) throws UnknownHostException{
        testClient = new RESTClient(host, port);
    }

    @AfterMethod
    public void tearDown(){}


    /**
     * простой тест на проверку изменения величины списка
     * после добавления
     * Добавляем, проверяем
     */
    @Test()
    public void getBearsLengthTest(){
        try{

            String deleteAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deleteAllOk, "OK");

            int responseCreateCode = testClient.createBear("POLAR", "Name1", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("POLAR", "Name2", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BROWN", "Name3", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BROWN", "Name4", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BLACK", "Name5", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("BLACK", "Name6", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("GUMMY", "Name7", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);
            responseCreateCode = testClient.createBear("GUMMY", "Name8", (float) 0.0);
            Assert.assertEquals(responseCreateCode, 200);

            final int okLength = 8;
            JSONArray afterBears = testClient.getAllBears();
            Assert.assertEquals(afterBears.length(), okLength);
        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }


    /**
     * тест повторяет логику из create теста, но всё же
     * 
     */
    @Test(dataProvider = "validBears")
    public void checkBearByIdTest(String bear_type, String bear_name, double bear_age){
        try{

            String deleteAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deleteAllOk, "OK");

            int responseCreateCode = testClient.createBear(bear_type, bear_name, (float) bear_age);
            Assert.assertEquals(responseCreateCode, 200);

            List<Integer> bearsIdList = testClient.getIdList();
            int newBearId = bearsIdList.get(0);

            JSONObject bearUnderTest = testClient.readBearById(newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_id"), newBearId);
            Assert.assertEquals(bearUnderTest.get("bear_type"), bear_type);
            Assert.assertEquals(bearUnderTest.get("bear_name"), bear_name);
            Assert.assertEquals(bearUnderTest.get("bear_age"), bear_age);
        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }


   /**
     * Проверяем что после создания и удаления по удаленногому id приходит ответ EMPTY
     * 
     */
    @Test(dataProvider = "validBears")
    public void checkBearByDeletedIdTest(String bear_type, String bear_name, double bear_age){
        try{

            String deleteAllOk = testClient.deleteAllBears();
            Assert.assertEquals(deleteAllOk, "OK");

            int responseCreateCode = testClient.createBear(bear_type, bear_name, (float) bear_age);
            Assert.assertEquals(responseCreateCode, 200);

            List<Integer> bearsIdList = testClient.getIdList();
            int newBearId = bearsIdList.get(0);

            String deletedByIdOk = testClient.deleteBearById(newBearId);
            Assert.assertEquals(deletedByIdOk, "OK");

            String deletedResponseText = testClient.readBearByDeletedId(newBearId);
            Assert.assertEquals(deletedResponseText, "EMPTY");
        } catch (Exception ex){
            System.out.println("Exception ex " + ex);
        }
    }

}