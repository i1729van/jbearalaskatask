# Bear tester

Для запуска самих тестов понадобится:
* java (8 or higher)
* gradle
* docker (только для запуска сервиса медведей)
    
## Preinstall steps:

### Изменяем параметры (адрес сервиса)
Для смены ip адреса и порта укажите значения в xml файлах(для каждого теста), находящийся в директории resources:
    
    jbearalaskatask/src/test/resources/blackboxed.xml
    jbearalaskatask/src/test/resources/whiteboxed.xml
    
### Запуск сервиса в контейнере 
Для запуска конейнера и получения доступа к порту сервиса (как раз по дефолту для тестов):

```sh 
$ docker run -p 8091:8091 azshoo/alaska:1.0
```
    
    


## Installation and Run:

  В директории проекта сборка и запуск выполняется командой:
  
```sh 
$ git clone https://gitlab.com/i1729van/jbearalaskatask.git
$ cd jbearalaskatask
$ gradle clean alaskabears -PXXX
```
где **XXX** - имя таска
**all** - все, и blackboxed и whiteboxed
**white** - только whiteboxed suite
**black** - только blackboxed suite

### Пример:
```sh 
$ cd jBearAlaskaTask
$ gradle clean alaskabears -Pwhite
```
